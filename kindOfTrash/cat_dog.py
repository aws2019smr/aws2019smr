#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 14:18:08 2019

@author: smriti
"""




#Return True if the string "cat" and "dog" appear the same number of times in the given string.



def cat_dog(str):
  count_c=0
  count_d=0
  for i in range(len(str)-1):
    if str[i:i+3]=="cat":
      count_c=count_c+1
    elif str[i:i+3]=="dog":
      count_d=count_d+1
    else:
      continue
  
  if count_c==count_d:
    return True
  else:
    return False
    