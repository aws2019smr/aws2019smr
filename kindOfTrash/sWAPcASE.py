#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 19:33:17 2019

@author: Smriti
"""

def swap_case(s):
    str=""
    for i in range(len(s)):
        x=s[i].isupper()
       # y=s[i].islower()
        if x is True:
            str=str+s[i].lower()
        else:
            str=str+s[i].upper()
    
    return str

if __name__ == '__main__':
    print(swap_case("Hey, I am Smriti"))