#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 16:27:30 2019

@author: Smriti
"""
'''
first line, print True if  has any alphanumeric characters. Otherwise, print False.
second line, print True if  has any alphabetical characters. Otherwise, print False.
third line, print True if  has any digits. Otherwise, print False.
fourth line, print True if  has any lowercase characters. Otherwise, print False.
fifth line, print True if  has any uppercase characters. Otherwise, print False.
'''
if __name__ == '__main__':
    s = input()
    a=0
    b=0
    c=0
    d=0
    e=0
    for i in range(len(s)):
        
        if s[i].isalnum() is True:
            a=a+1
        
        if s[i].isalpha() is True:
            b=b+1

        if s[i].isdigit() is True:
            c=c+1

        if s[i].islower() is True:
            d=d+1

        if s[i].isupper() is True:
            e=e+1
''' 
    print(a)
    print(b)
    print(c)
    print(d)
    print(e)
'''
    if a>=1:
        print(True)
    else:
        print(False)
    if b>=1:
        print(True)
    else:
        print(False)
    if c>=1:
        print(True)
    else:
        print(False)
    if d>=1:
        print(True)
    else:
        print(False)
    if e>=1:
        print(True)
    else:
        print(False)
            