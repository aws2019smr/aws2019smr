#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  4 18:26:28 2019

@author: smriti
"""

#Given a non-negative number "num", return True if num is within 2 of a multiple of 10.

def near_ten(num):
  a=num%10
  if (10-(10-a))<=2 or (10-a)<=2:
    return True
  else:
    return False