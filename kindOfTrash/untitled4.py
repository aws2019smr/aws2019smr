#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 19:07:09 2019

@author: smriti
"""

'''
str="notYour"
n=len(str)
x=str.startswith("not",0,n-1)
print(x)

a=str
b="not"
c=b+a
print(c)

'''



#Given a string, return a new string where "not " has been added to the front.
#However, if the string already begins with "not", return the string unchanged.



def not_string(str):
  n=len(str)
  x=str.startswith("not",0,n)
  b="not"
  
  
  if x is True:
    return str
  else:
    return b+" "+str



'''
def not_string(str):
  if len(str) >= 3 and str[:3] == "not":
    return str
  return "not " + str
  # str[:3] goes from the start of the string up to but not
  # including index 3
  
  '''