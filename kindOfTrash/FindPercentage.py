#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 18:58:11 2019

@author: smriti
"""


if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        #The * is being used to grab additional returns from the split statement.
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()
    
    print(student_marks)
    lst=student_marks[query_name]
    
    per= sum(lst)/len(lst)

#per=20   
    print("{0:.2f}".format(per))

    print("second","%.2f"%per)
    
    