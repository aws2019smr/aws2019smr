#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 21:09:20 2019

@author: smriti
"""

'''
We want make a package of goal kilos of chocolate. 
We have small bars (1 kilo each) and big bars (5 kilos each). 
Return the number of small bars to use, assuming we always use big bars before small bars. 
Return -1 if it can't be done.

'''


def make_chocolate(small, big, goal):
  
  if (goal%5)<=small and (goal-(big*5))<=small:
    if 5*big==goal:
      return 0
    elif 5*big>goal:
      if small==goal:
        return small
      else:
        return goal-5
    else:
      y=goal-(5*big)
      if y==small:
        return small
      else:
        return y
  else:
    return -1













