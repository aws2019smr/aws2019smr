#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 16:47:02 2019

@author: smriti
"""

'''

def sum13(nums):
  su=0
  y=len(nums)
  #print("length",y)
  for i in range(y-1):
    if nums[i]==13:
        nums[i]=0
        #print("value of i",i)
        if i<y-1:
          nums[i+1]=0
    su=su+nums[i]

  return su+nums[len(nums)-1]
'''

def sum13(nums):
  su=0
  y=len(nums)
  if y==0:
    return 0
  elif y>0:
    for i in range(y):
      if nums[i]==13:
        nums[i]=0
        if i<(y-1):
          nums[i+1]=0
      su=su+nums[i]

  return su

print(sum13([13, 5, 7, 2, 10]))