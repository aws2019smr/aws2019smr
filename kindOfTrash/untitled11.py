#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 15:39:22 2019

@author: smriti
"""
'''


Given a string, return the count of the number of times that a substring length 2 appears in the string and also as the last 2 chars of the string, so "hixxxhi" yields 1 (we won't count the end substring).


last2('hixxhi') → 1
last2('xaxxaxaxx') → 1
last2('axxxaaxx') → 2
'''




str="axxxaaxx"
n=len(str)
sstr=str[-2:]
print(sstr)
lstr=str[0:n-2]
print(lstr)
count=0
i=0
#print(lstr[i:i+2])


for i in range(n-1):
#    print(lstr[i:i+2])
#    print(sstr)
    if (sstr==lstr[i:i+2]):
        count=count+1

print(count)



'''
def last2(str):
  # Screen out too-short string case.
  if len(str) < 2:
    return 0
  
  # last 2 chars, can be written as str[-2:]
  last2 = str[len(str)-2:]
  count = 0
  
  # Check each substring length 2 starting at i
  for i in range(len(str)-2):
    sub = str[i:i+2]
    if sub == last2:
      count = count + 1

  return count
  
  '''