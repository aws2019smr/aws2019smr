#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 17:33:45 2019

@author: smriti
"""

#Return the sum of the numbers in the array, except ignore sections of numbers starting with a 6 and extending to the next 7 (every 6 will be followed by at least one 7). Return 0 for no numbers.


def sum67(nums):
  su=0
  flag=0
  for i in range(len(nums)):
    if flag==0:
      if nums[i]==6:
        flag =1
      else:
        su=su+nums[i]
    else:
      if nums[i]==7:
        flag=0
      else:
        continue
  return su
        