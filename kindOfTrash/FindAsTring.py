#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 15:09:48 2019

@author: Smriti
"""



def count_substring(string,sub_string):
    #count=string.count(sub_string)
    count=0
    start=0
    while start<len(string):
        flag=string.find(sub_string,start)
        #print(flag)
        if flag!=-1:
            start=flag+1
            count=count+1
        else:
            return count
    
    

if __name__=='__main__':
    string=input().strip()
    sub_string=input().strip()
    count=count_substring(string,sub_string)
    print(count)