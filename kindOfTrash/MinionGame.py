#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 16:59:18 2019

@author: Smriti

Kevin and Stuart want to play the 'The Minion Game'.

Game Rules

Both players are given the same string, .
Both players have to make substrings using the letters of the string .
Stuart has to make words starting with consonants.
Kevin has to make words starting with vowels.
The game ends when both players have made all possible substrings.

Scoring
A player gets +1 point for each occurrence of the substring in the string .

For Example:
String  = BANANA
Kevin's vowel beginning word = ANA
Here, ANA occurs twice in BANANA. Hence, Kevin will get 2 Points.

For better understanding, see the image below:

banana.png

Your task is to determine the winner of the game and their score.

Input Format

A single line of input containing the string .
Note: The string  will contain only uppercase letters: .

Constraints



Output Format

Print one line: the name of the winner and their score separated by a space.

If the game is a draw, print Draw.

Sample Input

BANANA
Sample Output

Stuart 12
Note :
Vowels are only defined as AEIOU . In this problem,  Y is not considered a vowel.

"""
def minion_game(str1):
    
    list1 = []
    for i in range(len(str1)):
        for j in range(i+1,len(str1)+1):
            list1.append(str1[i:j])
    
    #print(list1)
    
    KevinlistV = []
    StuartlistC = []
    
    for i in list1:
        #print(i)
        if i[0] =='A' or i[0] == 'E' or i[0] == 'I' or i[0] == 'O' or i[0] == 'U':
            KevinlistV.append(i)
        else:
            StuartlistC.append(i)
            
    
    KevinlistV = list(dict.fromkeys(KevinlistV)) #remove duplicates
    StuartlistC = list(dict.fromkeys(StuartlistC))
    
    #print("Kevin's List  ", KevinlistV)
    #print("Stuart's List   ", StuartlistC)
    
    
    kevin=0
    stuart=0
    
    for x in KevinlistV:
        y=len(x)
        for i in range(len(str1)):
            if str1[i:i+y] == x:
                kevin = kevin +1
                
    for x in StuartlistC:
        y=len(x)
        for i in range(len(str1)):
            if str1[i:i+y] == x:
                stuart = stuart +1
                   
    #print(kevin)
    #print(stuart)
    if kevin > stuart :
        print("Kevin {:d}".format(kevin))
    elif stuart > kevin:
        print("Stuart {:d}".format(stuart))
    else:
        print("Draw")
    
    
    
    
    
if __name__ == '__main__':
    s = input()
    minion_game(s)