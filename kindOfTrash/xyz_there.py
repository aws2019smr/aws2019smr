#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 14:51:16 2019

@author: smriti
"""


#Return True if the given string contains an appearance of "xyz"
# where the xyz is not directly preceeded by a period (.). 
#So "xxyz" counts but "x.xyz" does not.



def xyz_there(str):
    for i in range(len(str)):
        if str[i] != '.' and str[i+1:i+4] == 'xyz':
            return True
    if str[0:3] == "xyz":
        return True
    return False
          
  
print(xyz_there("abc.xyzxyz"))        