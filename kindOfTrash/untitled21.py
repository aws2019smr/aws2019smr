#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 19:43:46 2019

@author: smriti
"""
'''
 we'll round an int value up to the next multiple of 10 if its rightmost digit is 5 or more, 
 so 15 rounds up to 20. Alternately, round down to the previous multiple of 10 
 if its rightmost digit is less than 5, so 12 rounds down to 10. 
 Given 3 ints, a b c, return the sum of their rounded values. To avoid code repetition,
 write a separate helper "def round10(num):" and call it 3 times. 
 Write the helper entirely below and at the same indent level as round_sum().

'''

def round_sum(a, b, c):
  return round10(a)+round10(b)+round10(c)

def round10(num):
  x=num%10
  if x==0:
    return num
  elif x!=0 and x<5:
    return num-x
  else:
    y=10-x
    return num+y 



print(round_sum(6,4,4))

