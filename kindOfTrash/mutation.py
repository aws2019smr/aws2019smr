#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 14:54:04 2019

@author: Smriti
"""

def mutate_string(string,position,character):
    y=list(string)
    y[position]=character
    x="".join(y)
    return x

if __name__=='__main__':
    s=input()
    i,c=input().split()
    new_s=mutate_string(s,int(i),c)
    print(new_s)


