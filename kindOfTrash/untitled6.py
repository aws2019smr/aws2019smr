#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 19:49:19 2019

@author: smriti
"""

# Given a string, return a new string where the first and last chars have been exchanged.




def front_back(str):
  x=len(str)
  return (str[-1:]+str[1:x-1]+str[:-(x-1)])

print(front_back("Smriti"))