#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 19:14:10 2019

@author: smriti
""" 

#Given a non-empty string and an int n, 
#return a new string where the char at index n has been removed.
#The value of n will be a valid index of a char in the original string 
#(i.e. n will be in the range 0..len(str)-1 inclusive).


def missing_char(str, n):
      print (str[:n]+str[n+1:])
  
missing_char("SMRITI",3)