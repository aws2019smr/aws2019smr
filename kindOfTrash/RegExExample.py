#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 14:47:49 2019

@author: smriti
"""

import re

txt="The rain in Spain"
x=re.search(r"^The.*Spain$",txt)

if (x):
  print("YES! We have a match!", x.group())
else:
  print("No match")