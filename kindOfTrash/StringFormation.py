#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 19:52:05 2019

@author: Smriti

Given an integer, , print the following values for each integer  from  to :

Decimal
Octal
Hexadecimal (capitalized)
Binary
The four values must be printed on a single line in the order specified above for each  from  to . Each value should be space-padded to match the width of the binary value of .

Input Format

A single integer denoting .

Constraints

Output Format

Print  lines where each line  (in the range ) contains the respective decimal, octal, capitalized hexadecimal, and binary values of . Each printed value must be formatted to the width of the binary value of .

Sample Input

17
Sample Output

    1     1     1     1
    2     2     2    10
    3     3     3    11
    4     4     4   100
    5     5     5   101
    6     6     6   110
    7     7     7   111
    8    10     8  1000
    9    11     9  1001
   10    12     A  1010
   11    13     B  1011
   12    14     C  1100
   13    15     D  1101
   14    16     E  1110
   15    17     F  1111
   16    20    10 10000
   17    21    11 10001     



def print_formatted(number):
    # your code goes here
    w = len(format(number, 'b'))
    for i in range(1, number+1):
        d = str(i).rjust(w)
        o = format(i, 'o').rjust(w)
        h = format(i, 'X').rjust(w)
        b = format(i, 'b').rjust(w)
        print(d, o, h, b)
    
if __name__ == '__main__':
    n = int(input())
    print_formatted(n)

"""




def print_formatted(number):
    x = [None] * number
    w = len(format(number,'b'))
    for i in range(1,number+1):
        dec = str(i).rjust(w)
        oc = oct(i)[2:].rjust(w)
        hx = hex(i)[2:].upper().rjust(w)
        bn = bin(i)[2:].rjust(w)
      #  x = x + [[num1,num2[2:],num3[2:],num4[2:]]]
        x[i-1] = [dec,oc,hx,bn]
        #print (((x[i-1][0]).rjust(w))+ ((x[i-1][1]).rjust(w)) + ((x[i-1][2]).rjust(w)) + ((x[i-1][3]).rjust(w)))
        print (x[i-1][0],x[i-1][1], x[i-1][2], x[i-1][3])
    
    
   # print(w)
   # print(x.elements)
   # for i in range(len(x)):
     #    print (((x[i][0]).rjust(0))+ ((x[i][1]).rjust(5)) + ((x[i][2]).rjust(5)) + ((x[i][3]).rjust(5)))
         
    
    
    #pattern=(((x[i][0]).rjust(0))+ ((x[i][1]).rjust(5)) + ((x[i][2]).rjust(5)) + ((x[i][3]).rjust(5)) for i in range(len(x)))
    
    #return pattern
        
    # your code goes here

if __name__ == "__main__":
    n = int(input())
    x = print_formatted(n)
    
    

    
  