#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 14:49:37 2019

@author: Smriti
Using juggling algo
"""





    
def leftRotate(arr, d): 
    n=len(arr)
    g_c_d = gcd(d, n) 
    for i in range(g_c_d): 
          
        # move i-th values of blocks  
        temp = arr[i] 
        j = i 
        while 1: 
            k = j + d 
            if k >= n: 
                k = k - n 
            if k == i: 
                break
            arr[j] = arr[k] 
            j = k 
        arr[j] = temp


def gcd(a, b): 
    if b == 0: 
        return a; 
    else: 
        return gcd(b, a % b) 
    

def printarr(arr):
    size=len(arr)
    for i in range(size):
        print(arr[i])
'''
def printArray(arr): 
    size=len(arr)
    for i in range(size): 
        print ("% d" % arr[i], end =" ") 
'''
        
arr=[1,2,3,4,5,6,7]

leftRotate(arr,2)
printarr(arr)

