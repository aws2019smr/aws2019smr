#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 13:52:13 2019

@author: smriti
"""

#Given a string, return a string where for every char in the original, there are two chars.

def double_char(str):
  x=len(str)
  y=""
  for i in range(x):
    y=y+str[i]+str[i]
  return y
  