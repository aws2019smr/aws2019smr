#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 02:02:32 2019

@author: smriti
"""

#We want to make a row of bricks that is goal inches long.
#We have a number of small bricks (1 inch each) and big bricks (5 inches each).
#Return True if it is possible to make the goal by choosing from the given bricks. 

def make_bricks(small, big, goal):
  if (goal%5)<=small and (goal-(big*5))<=small:
    return True
  else:
    return False
 
    
