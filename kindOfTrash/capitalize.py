#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 14:48:12 2019

@author: Smriti
"""
import math
import os
import random
import re
import sys

s=input()

x = s.title()
# capitalize() returns copy with only first word's first character capitalize
# title() : it capitalize first character of every word in string
for x in s[:].split():
    s=s.replace(x,x.capitalize())
print(s)


