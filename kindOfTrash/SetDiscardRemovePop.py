#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 16:31:20 2019

@author: Smriti

.remove(x)
This operation removes element  from the set.
If element  does not exist, it raises a KeyError.
The .remove(x) operation returns None.

Example

>>> s = set([1, 2, 3, 4, 5, 6, 7, 8, 9])
>>> s.remove(5)
>>> print s
set([1, 2, 3, 4, 6, 7, 8, 9])
>>> print s.remove(4)
None
>>> print s
set([1, 2, 3, 6, 7, 8, 9])
>>> s.remove(0)
KeyError: 0
.discard(x)
This operation also removes element  from the set.
If element  does not exist, it does not raise a KeyError.
The .discard(x) operation returns None.

Example

>>> s = set([1, 2, 3, 4, 5, 6, 7, 8, 9])
>>> s.discard(5)
>>> print s
set([1, 2, 3, 4, 6, 7, 8, 9])
>>> print s.discard(4)
None
>>> print s
set([1, 2, 3, 6, 7, 8, 9])
>>> s.discard(0)
>>> print s
set([1, 2, 3, 6, 7, 8, 9])
.pop()
This operation removes and return an arbitrary element from the set.
If there are no elements to remove, it raises a KeyError.

Example

>>> s = set([1])
>>> print s.pop()
1
>>> print s
set([])
>>> print s.pop()
KeyError: pop from an empty set
Task
You have a non-empty set , and you have to execute  commands given in  lines.

The commands will be pop, remove and discard.

Input Format

The first line contains integer , the number of elements in the set .
The second line contains  space separated elements of set . All of the elements are non-negative integers, less than or equal to 9.
The third line contains integer , the number of commands.
The next  lines contains either pop, remove and/or discard commands followed by their associated value.

Constraints



Output Format

Print the sum of the elements of set  on a single line.

Sample Input

9
1 2 3 4 5 6 7 8 9
10
pop
remove 9
discard 9
discard 8
remove 7
pop 
discard 6
remove 5
pop 
discard 5
Sample Output

4
Explanation

After completing these  operations on the set, we get set. Hence, the sum is .

Note: Convert the elements of set s to integers while you are assigning them. To ensure the proper input of the set, we have added the first two lines of code to the editor.

"""

n = int(input())
s = set(map(int,input().split()))

N = int(input())
action_array=[]
print(s)
for i in range(N):
    command , *x = input().split()
    lenx = len(x)
    #print(lenx)
    dict1 = {}
    if lenx == 1:
        dict1 = {command:{"value":x[0]}}
    else:
        dict1 = {command:""}
    
    action_array.append(dict1)
    print("action_array",action_array)


for dict1 in action_array:
    key = list(dict1.keys())[0] # list of all the keys in the dictionary.
    print("key",key)
    
    if key == "pop":
        if len(s)>0:
            s.pop()
            print("set after pop: ",s)
        
    elif key == "discard":
        int_dict = dict1.get(key)
        print("int_dict 1:",int_dict)
        value = int(int_dict.get("value"))
        print("value 1:",value)
        s.discard(value)
        print("set after discard : ", s)
    elif key == "remove":
        int_dict = dict1.get(key)
        print("int_dict 2:",int_dict)
        value = int(int_dict.get("value"))
        print("value 2:",value)
        if value in s:
            s.remove(value)
        print("set after remove : ", s )
        

print(s)
x=list(s)
print(sum(x))
        





