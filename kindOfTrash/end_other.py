#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 14:40:11 2019

@author: smriti
"""
'''
Given two strings, 
return True if either of the strings appears at the very end of the other string,
 ignoring upper/lower case differences 
 (in other words, the computation should not be "case sensitive"). 
 Note: s.lower() returns the lowercase version of a string
'''

def end_other(a, b):
  c=a.lower()
  d=b.lower()
  if c.endswith(d):
    return True
  elif d.endswith(c):
    return True
  else:
    return False