#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 19:51:26 2019

@author: Smriti
"""

def print_full_name(a, b):
    x="Hello {} {}! You just delved into python."
    print("\n"+ x.format(a,b))

if __name__ == '__main__':
    first_name = input()
    last_name = input()
    print_full_name(first_name, last_name)
    