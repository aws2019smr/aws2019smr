#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  3 17:54:07 2019

@author: smriti
"""

#Given an array of ints length 3, figure out which is larger, 
#the first or last element in the array, and set all the other elements to be that value. Return the changed array.


nums=[6,8,10]

big=max(nums[0],nums[2])
nums[0]=big
nums[1]=big
nums[2]=big
print(nums)