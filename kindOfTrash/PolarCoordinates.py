#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 15 18:16:12 2019

@author: Smriti
"""
import cmath

x = 1+2j

print(cmath.phase(x))

#print(cmath.phase(complex(x)))

#both has same output


a = x.real # real part
b = x.imag #imaginary part

print(cmath.sqrt((a*a)+(b*b)))

#print(complex(x))

z = cmath.sqrt((a*a)+(b*b))

print(z.real)