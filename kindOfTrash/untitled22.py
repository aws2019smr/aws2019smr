#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 20:15:09 2019

@author: smriti
"""
'''

Given three ints, a b c, return True if one of b or c is "close"
 (differing from a by at most 1), while the other is "far", differing from both other values
 by 2 or more. Note: abs(num) computes the absolute value of a number.

'''




def close_far(a, b, c):
 if abs(b-a) <= 1:
  close = b
 elif abs(c-a) <= 1:
  close = c
 

 
 if (close == b) and (abs(c-a) >= 2) and (abs(c-b)>=2):
  return True
 elif (close == c) and (abs(b-a) >= 2) and (abs(b-c)>=2):
  return True
 else:
  return False

print(close_far(10,10,8))