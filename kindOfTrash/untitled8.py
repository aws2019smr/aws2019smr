#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 14:27:12 2019

@author: smriti
"""

# Given a string and a non-negative int n
#return a larger string that is n copies of the original string.


def string_times(str, n):
  x=""
  for i in range(n):
    x=str+x
  print (x)

string_times("smriti",2)


'''

#Given a string and a non-negative int n, 
#we'll say that the front of the string is the first 3 chars, or whatever is there 
#if the string is less than length 3. Return n copies of the front;


def front_times(str, n):
  x=""
  for i in range(n):
    x=str[:3]+x
  return x
'''