#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 14:22:36 2019

@author: smriti
"""

#Return the number of times that the string "code" appears anywhere in the given string, 
#except we'll accept any letter for the 'd', so "cope" and "cooe" count.


str="coAcodeBcoleccoreDD"
count=0
for i in range(len(str)-3):
#    print("A",str[i:i+2])
#    print("B",str[i+3])
    if str[i:i+2]=="co" and str[i+3]=="e":
        count=count+1
    else:
        continue
print("Count",count)