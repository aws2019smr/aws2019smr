print("passing parameters in function")


def my_fun(fname):
	print(fname + " yayyyy ")

my_fun("hello")
my_fun("world")
my_fun("Smriti")


print("passing a list as a parameter")

def my_fun1(food):
	for x in food:
		print(x)

fruits=["apple","banana","cherry"]

my_fun1(fruits)


