#conditions

# use whitespaces to define scope of code

print("using if keyword")
a=22
b=22

if b>a:
	print("b is greater than a")
else:
	print("b is smaller than a")

print("using elif keyword")

x=100
y=100

if y>x:
	print("y is greater than x")

elif x==y:
	print("x is equals to y")


print("using else")

x=11
y=2

if y>x:
	print("y is greater than x")
elif x==y:
	print("x equals to y")
else:
	print("x is greater than y")


print("shorthand if, else command: works only on updated python3")

print("A") if x > y else print("B")


print("A") if b>a else print("=") if a==b else print("B")

# and & or logical operators

print ("using and & or logical operator")

a=10
b=20
c=5

if a>b or a>c:
	print("Atleast one of the conditions is true")

if a>c and b>a:
	print("Both conditions are true")


