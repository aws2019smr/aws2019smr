
print("lambda with anonymous function")

print(" Use lambda function when an anonymous function is required for a short period")

def my_f(n):
	return lambda a:a*n

doubler=my_f(2)
tripler=my_f(3)

print(doubler(11))
print(tripler(11))
