
# LAMBDA : small anonymous function. can take any no. of arguments but can have only one expression.


# lambda arguments: expressions  (Syntax)


print("adds 10 to the number passed as argument")

x=lambda a:a+10

print(x(5))

print("multiple arguments")
x=lambda a,b:a+b
print(x(6,8))

x=lambda a,b,c:a+b+c

print(x(2,6,8))



