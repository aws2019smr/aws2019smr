#N  O  T  E

#TUPLE: collection which is ordered and unchangeable and written with round brackets.


thistuple = ("ape","bat","cat")
print("this tuple  ",thistuple)


#access tuple items

print(thistuple[1])

#loop thru a tuple

print("Printing loop")

for x in thistuple:
	print(x)


#Check if item exists

if "cat" in thistuple:
	print ("yes, CAT is in the tuple")


#Tuple length

print("length :", len(thistuple))

#tuple constructor 

newtuple = tuple(("apple","banana","cherry"))
print("new tuple   :",newtuple)



# count() and index()

#count() return the no. of times a value appears in a tupple

tuple1=(1,2,3,2,4,5,6,3,5,1,5,3,5,0,9)
x= tuple1.count(5)
print("no. of times 5 comes in tuple :",x)

#index() search the first occurance of value, and returns its position.

y = tuple1.index(9)

print("9 is at position :",y)


