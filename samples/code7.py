#S E T S : 

# collection which is unordered and unindexed, you cant change its items but you can add new ones.


thiset= {"A","B","C","D","E","F","S","M"}

print("Printing set   :", thiset)


for x in thiset:
	print(x)

print("S" in thiset)


print("adding new item")

thiset.add("smriti")

print(thiset)

#add multiple items

print("adding and printing multiple items")

thiset.update(["Z","Y","V"])

print(thiset)

print("length of set  ",len(thiset))

thiset.remove("D")

print("removing D   ",thiset)



#Note: Sets are unordered, so when using the pop() method, you will not know which item that gets removed.



