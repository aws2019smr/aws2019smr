
#WHILE LOOPS

i=0

while i<6:
	print(i)
	i+=1

print("another code with break")
i=1

while i<6:
	print(i)
	if i==3:
		print("hey")
		break
	i+=1


print("code with continue")

i=0

while i<6:
	i+=1
	if i==3:
#		continue
		print("Hey, continue")
		continue
	print(i)

