#FOR LOOP

fruits=["apple","banana","cherry"]

for x in fruits:
	if x=="banana":
		continue
	print(x)


print("range() function returns a sequence of numbers, from 0 by default")
print("increments by 1 and ends at specified number")

for x in range(6):
	print(x)


print("range 2,6")

for x in range(2,6):
	print(x)


#the third parameter specifies the value increment.


print("range 2,30,3")

for x in range(2,30,3):
	print(x)


print("else in for loop")


for x in range(5):
	print(x)
else:
	print("Finished")


animals=["ape","bat","cat","dog"]


for x in animals:
	for y in fruits:
		print(x,y)


