
# INHERITANCE

#it allows us to define a class that inhertits all the methods and properties from another class.

class A:
	def __init__(self,fname,lname):
		self.firstname = fname
		self.lastname = lname

	def printname(self):
		print(self.firstname,self.lastname)


x= A("Smriti","P")
x.printname()

#to create a class that inherits the functionality from another class, ....
# send the parent class as a parameter when creating the child class.


#use "pass" keyword when you don't want to add any other properties or methods to the class.


class S(A):
	pass

x=S("Happy","Olsen")
x.printname()

# add __init__() function

#Child's init function overrides the inheritance of parent's init function.
#to keep the inheritance of init function, add a call to the parent's init function.

class B(A):
	def __init__(self,fname,lname):
	A.__init__(self,fname,lname)


x = B("Jack","Jill")
x.printname()

#this code has error.



