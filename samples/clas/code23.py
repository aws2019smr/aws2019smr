

class A:
 def __init__(self,fname,lname):
  self.firstname=fname
  self.lastname=lname


 def printname(self):
  print(self.firstname,self.lastname)


class B(A):
 def __init__(self,fname,lname,year):
  A.__init__(self,fname,lname)
  self.graduationyear = 2019

 def wc(self):
  print("WC", self.firstname,self.lastname, " to the class of", self.graduationyear)

x=B("WO","HO",2019)
x.wc()
