
# C L A S S

print("CLASSES & OBJECTS")

#class is like an object constructor 

class MyClass:
	x=5
print(MyClass)

p2=MyClass() #create an object

print(p2.x)

print("The Built-in __init_() Function")

# init function is always executed when the class is being initiated
# use it to assign values to object properties 
# or other operations that are necessary to do when the obj is being created.

class Person:
	def __init__(self,name,age):
		self.name=name
		self.age=age

#self is refrence to the current instance of class. 
#it ds not hs to be "self" but must be first parameter of any function

p1=Person("Smriti",5)

print(p1.name)
print(p1.age)

#Object methods


class School:
	def __init__(self,name,age):
		self.name=name
		self.age=age

	def myfun(self):
		print("Hello World, I am "+ self.name )

p3=School("Manish",2)
p3.myfun()

p3.age=50

print(p3.age)

del p1.age #delete age

del p1 # delete object
