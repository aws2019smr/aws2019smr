

# ITERATOR AND ITERABLE

# __iter__()  and __next__()

# list , tuple, dictionaries and sets are all iterable objects.
# they have iter() method which is used to get an iterator.

mytuple = ("ape","bat","cat")

myit = iter(mytuple)

print(next(myit))

print(next(myit))

print(next(myit))

#strings are iterable objects and can return an iterator.

mystr="SMRITI"
myit=iter(mystr)

print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))
print(next(myit))


#looping through an iterator

for x in mytuple:
 print(x)



for x in mystr:
 print(x)


