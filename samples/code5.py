
#DATATYPES

#List

thisWonderfulList=["apple","banana","cherry"]

print("Printing a list       ",thisWonderfulList)

print("I am first on list      ",thisWonderfulList[0])

thisWonderfulList[0]="Apricot"

print("Updated list",thisWonderfulList)

if "banana" in thisWonderfulList:
	print("yayy..banana found")

print("List length  :",len(thisWonderfulList))

print("adding items to list now")

thisWonderfulList.append("Dragon Fruit")

print(" updated items  ", thisWonderfulList)

thisWonderfulList.insert(1,"baywatch")

print(thisWonderfulList)

thisWonderfulList.remove("cherry")

print(thisWonderfulList)


#The pop() method removes the specified index, (or the last item if index is not specified):



print ("               ")

thisWonderfulList.pop(1)

print(thisWonderfulList)

#The del keyword removes the specified index, can also delete the list completely if index not specified.

del thisWonderfulList[0]

print (thisWonderfulList)


# clear() method empties the list

thisWonderfulList.clear() # // not working, throwing error

print("clearing list")

print(thisWonderfulList)

'''
thisWonderfulList.insert(0,"Apricot")

thisWonderfulList.insert(2,"Cherry")

print(thisWonderfulList)



# COPYING A LIST 


mylist=list(thisWonderfulList)

print("mylist", mylist)

'''
