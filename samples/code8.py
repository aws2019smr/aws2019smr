
#DICTIONARIES

#unordered collection, changable and indexed, they have keys and values.

thisdict = {
  "brand": "FORD",
  "model": "MUSTANG",
  "year": 1964
}

print(thisdict)

print("dictionary length:",len(thisdict))

#accessing item

x=thisdict["model"]
print(x)

print("by using get method")

x=thisdict.get("model")

print(x)

#Changing values

print("changing values")

thisdict["year"]=1901
print(thisdict)
#Loop through a dictionary

print("Printing all key names in the dictionary one by one")

for x in thisdict:
	print(x)


print("printing all values")


for x in thisdict:
	print(thisdict[x])

#printing values using values function

print("printing values using values function")

for x in thisdict.values():
	print(x)


print("loop through both keys and values")

for x,y in thisdict.items():
	print(x,y)



#Checking key exixtance:

print("check if a key exists")


if "model" in thisdict:
	print("yes, its here")

print("adding items")


thisdict["color"]="pink"
print(thisdict)


thisdict["seats"]=4

print(thisdict)


#Copy a dictionary

print("copying dictionary")

newdict =thisdict.copy()
print("printing newdict dictionary",newdict)

#copy dictionary using built-in method dict()

mydict=dict(newdict)

print("copying newdict to mydict using dict()  ",mydict)

# The dict() constructor

alldict = dict(name="Smriti", age=5)
print("printing newly created dictionary using dict() ",alldict)

#########


print("removing items")


thisdict.pop("model")
print(thisdict)


#The popitem() method removes the last inserted item (in versions before 3.7, a random item is removed instead):

thisdict.popitem()
print(thisdict)


print("del keyword to remove specified key name")

del thisdict["seats"]
print(thisdict)


print("clear() to empties dictionary")

thisdict.clear()
print(thisdict)




